#!/bin/bash

set -ex

docker build -t devops:test app/.

cid=$(docker run -d -p 6789:5000 devops:test)

until test=$(curl localhost:6789 | grep 'Index!') &> /dev/null
do
    sleep 0.1
done

# Clean Docker
docker rm -fv "$cid" &> /dev/null

if [ $test == Index! ]; then
    echo "Test Passes! Smile :)"
else
    echo "Test Fails, Try again!"
    exit 1
fi
