
from __future__ import print_function
import argparse
import logging
import sys
import boto3
import botocore

log = logging.getLogger('cfn_stack')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--stack-name',
                        default='devops-stack',
                        help='The CFN stack to update')
    parser.add_argument('--region',
                        default='eu-west-1',
                        help='The AWS region to deploy')
    parser.add_argument('--docker-image',
                        default='devops_docker',
                        help='Container version to be deployed')
    parser.add_argument('--template',
                        default='stack.json',
                        help='The CFN Template to update the stack')
    parser.add_argument('--webserverport',
                        default='5000',
                        help='TCP Port to listen in')
    parser.add_argument('--keyname',
                        default='chadell_aws',
                        help='SSH key')


    args = parser.parse_args()

    log_level = logging.INFO
    logging.basicConfig(stream=sys.stdout, level=log_level)

    log.info(args)


    client = boto3.client('cloudformation')

    list_of_stacks = client.list_stacks()

    create = True
    for stack in list_of_stacks['StackSummaries']:
        if args.stack_name in stack['StackId'] and stack['StackStatus'] != 'DELETE_COMPLETE':
            create = False

    log.info(create)
    if create:
        client.create_stack(
            StackName=args.stack_name,
            # TargetState='present',
            TemplateBody=open(args.template, 'r').read(),
            Capabilities=[
                'CAPABILITY_NAMED_IAM'
            ],
	    Parameters=[
                {
                    'ParameterKey': 'DockerImage',
                    'ParameterValue': args.docker_image,
                },
                {
                    'ParameterKey': 'WebServerPort',
                    'ParameterValue': args.webserverport,
                },
                {
                    'ParameterKey': 'KeyName',
                    'ParameterValue': args.keyname,
                }
            ],
       )
    else:
        try:
            client.update_stack(
                StackName=args.stack_name,
                TemplateBody=open(args.template, 'r').read(),
                Capabilities=[
                    'CAPABILITY_NAMED_IAM'
                ],
            )
        except botocore.exceptions.ClientError as ex:
            log.debug(ex)

if __name__ == "__main__":
    main()
